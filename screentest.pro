#-------------------------------------------------
#
# Project created by QtCreator 2012-11-19T15:49:12
#
#-------------------------------------------------

QT       += core gui

TARGET = screentest
TEMPLATE = app


SOURCES += main.cpp\
        screentestwindow.cpp

HEADERS  += screentestwindow.h

RESOURCES += \
    resource.qrc

ICON = icon.icns
