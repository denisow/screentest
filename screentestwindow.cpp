#include "screentestwindow.h"

ScreenTestWindow::ScreenTestWindow(QWidget *parent) :
    QWidget(parent)
{
    this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);
    this->setCursor(Qt::BlankCursor);
    this->screenType = WHITETEST;
    qApp->setWindowIcon(QIcon(":/img/icon.png"));
}

ScreenTestWindow::~ScreenTestWindow()
{
}

void ScreenTestWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Q || event->key() == Qt::Key_Escape){
        qApp->quit();
    }
    this->nextScreen( (event->key() == Qt::Key_Left) );
}

void ScreenTestWindow::mousePressEvent(QMouseEvent *event)
{
    this->nextScreen( (event->button() == Qt::RightButton) );
}

void ScreenTestWindow::nextScreen(bool revers)
{
    if(revers){
        this->screenType--;
    }else{
        this->screenType++;
    }
    this->update();
    if(this->screenType == EXIT || this->screenType == EXITBACK){
        qApp->quit();
    }
}

void ScreenTestWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    if(this->screenType == WHITETEST || this->screenType ==  DOPPLEREFFECTVERTICAL || this->screenType ==  DOPPLEREFFECTHORIZONTAL){
        painter.setBrush(Qt::white);
    }else if(this->screenType == REDTEST){
        painter.setBrush(Qt::red);
    }else if(this->screenType == GREENTEST){
        painter.setBrush(Qt::green);
    }else if(this->screenType == BLUETEST){
        painter.setBrush(Qt::blue);
    }else if(this->screenType == BLACKTEST){
        painter.setBrush(Qt::black);
    }
    painter.drawRect(rect());

    if(this->screenType ==  DOPPLEREFFECTHORIZONTAL){
        for(int i=0; i<rect().height()/2 ;i++){
            painter.setPen(QPen(Qt::black,1));
            painter.drawLine(QPoint(0,i*2), QPoint(rect().width(),i*2));
        }
    }
    if(this->screenType ==  DOPPLEREFFECTVERTICAL){
        for(int i=0; i<rect().width()/2 ;i++){
            painter.setPen(QPen(Qt::black,1));
            painter.drawLine(QPoint(i*2,0), QPoint(i*2,rect().height()));
        }
    }

}
