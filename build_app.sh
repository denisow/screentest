#!/bin/bash


APP_NAME="screentest"
APP_NAME_FOR_DMG="ScreenTest"
PATH_BG_IMG="dmg_bg.png"
PATH_ICN_IMG="dmgIcon.icns"


APP_X=130
APP_Y=200

APPS_X=470
APPS_Y=200

WINDOW_WIDTH=600
WINDOW_HEIGHT=400
	
WINDOW_LEFT=200
WINDOW_TOP=100

PATH_SOURCEAPP=../${APP_NAME}-build-desktop-Desktop_Qt_4_7_4_for_GCC__Qt_SDK__Debug/${APP_NAME}.app
PATH_APP=to_deployment/${APP_NAME}.app
PATH_PKG=to_deployment/${APP_NAME}.pkg
PATH_DMG=to_deployment/${APP_NAME}.dmg
PATH_DMG_TMP=to_deployment/${APP_NAME}_tmp.dmg
PATH_TOPACK=to_deployment/
PATH_QTLIBS=/Users/admin/QtSDK/Desktop/Qt/474/gcc/lib
PATH_APPLIBS=$PATH_APP/Contents/Frameworks

WINDOW_RIGHT=$((${WINDOW_LEFT}+${WINDOW_WIDTH}))
WINDOW_BOTTOM=$((${WINDOW_TOP}+${WINDOW_HEIGHT}))-3

echo $VOL_NAME

echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

echo "copy app file"
rm -rf $PATH_APP
rm -rf $PATH_PKG
rm -rf $PATH_DMG
cp -R $PATH_SOURCEAPP to_deployment

otool -L  $PATH_APP/Contents/MacOS/${APP_NAME}
echo "----------------------------------"


echo "copy frameworks"
mkdir $PATH_APPLIBS



# copy libs from local template 
cp -R Frameworks/QtCore.framework $PATH_APPLIBS
cp -R Frameworks/QtGui.framework $PATH_APPLIBS


echo "copy info.plist"
cp info.plist $PATH_APP/Contents


APP_VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "${PATH_APP}/Contents/Info.plist"`
#APP_BUILD_VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "${PATH_APP}/Contents/Info.plist"`
VOL_NAME="${APP_NAME_FOR_DMG} ${APP_VERSION}"

# change path of libs in 

install_name_tool -change $PATH_QTLIBS/QtCore.framework/Versions/4/QtCore\
					@executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore\
					$PATH_APP/Contents/MacOS/${APP_NAME}

install_name_tool -change $PATH_QTLIBS/QtGui.framework/Versions/4/QtGui\
					@executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui\
					$PATH_APP/Contents/MacOS/${APP_NAME}


# change path of libs in another libs

install_name_tool -change $PATH_QTLIBS/QtCore.framework/Versions/4/QtCore\
					@executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore\
					$PATH_APPLIBS/QtGui.framework/Versions/4/QtGui
										
					
					
#find . -name '*.DS_Store' -type f -delete




echo "----------------------------------"
otool -L  $PATH_APP/Contents/MacOS/${APP_NAME}

echo "codesign ... "
codesign -f -v -s "3rd Party Mac Developer Application: Pavel Denisov" --entitlements sandbox.entitlements $PATH_APPLIBS/QtGui.framework/Versions/4/QtGui
codesign -f -v -s "3rd Party Mac Developer Application: Pavel Denisov" --entitlements sandbox.entitlements $PATH_QTLIBS/QtCore.framework/Versions/4/QtCore
codesign -f -v -s "3rd Party Mac Developer Application: Pavel Denisov" --entitlements sandbox.entitlements "$PATH_APP"



echo "build dmg ... "
hdiutil create -ov -srcfolder $PATH_TOPACK -format UDRW -volname "${VOL_NAME}" "${PATH_DMG_TMP}"
device=$(hdiutil attach -readwrite -noverify -noautoopen ${PATH_DMG_TMP} | egrep '^/dev/' | sed 1q | awk '{print $1}')

PATH_BG="/Volumes/${VOL_NAME}/.background"
PATH_DMG_MOUNTED="/Volumes/${VOL_NAME}"

chmod -R u+w /Volumes/"${VOL_NAME}"

mkdir "${PATH_BG}"
cp "${PATH_BG_IMG}" "${PATH_BG}/"
cp "${PATH_ICN_IMG}" "${PATH_DMG_MOUNTED}/.VolumeIcon.icns"

APPLESCRIPT="
tell application \"Finder\"
    tell disk \"${VOL_NAME}\"
        open
        -- Setting view options
        set current view of container window to icon view
        set toolbar visible of container window to false
        set statusbar visible of container window to false
        set the bounds of container window to {${WINDOW_LEFT}, ${WINDOW_TOP}, ${WINDOW_RIGHT}, ${WINDOW_BOTTOM}}
        set theViewOptions to the icon view options of container window
        set arrangement of theViewOptions to not arranged
        set icon size of theViewOptions to 72
        -- Settings background
        set background picture of theViewOptions to file \".background:${PATH_BG_IMG}\"
        -- Adding symlink to /Applications
        make new alias file at container window to POSIX file \"/Applications\" with properties {name:\"Applications\"}
        -- Reopening
        close
        open
        -- Rearranging
        set the position of item \"Applications\" to {${APPS_X}, ${APPS_Y}}
        set the position of item \"${APP_NAME}.app\" to {${APP_X}, ${APP_Y}}
        -- Updating and sleeping for 5 secs
        update without registering applications
        delay 5
    end tell
end tell
"

echo "$APPLESCRIPT" | osascript

SetFile -c icnC "${PATH_DMG_MOUNTED}/.VolumeIcon.icns"
SetFile -a C "${PATH_DMG_MOUNTED}"

chmod -Rf go-w /Volumes/"${VOL_NAME}"
sync
hdiutil detach ${device}
hdiutil convert "${PATH_DMG_TMP}" -format UDZO -imagekey zlib-level=9 -o "${PATH_DMG}"
rm -rf $PATH_DMG_TMP


echo "build pkg ... "
productbuild --component "$PATH_APP" /Applications --sign "3rd Party Mac Developer Installer: Pavel Denisov" --product "$PATH_APP/Contents/Info.plist" $PATH_PKG


# test install: sudo installer -store -pkg to_deployment/screentest.pkg -target /
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo