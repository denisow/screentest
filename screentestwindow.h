#ifndef SCREENTESTWINDOW_H
#define SCREENTESTWINDOW_H
#define EXITBACK -1
#define WHITETEST 0
#define REDTEST 1
#define GREENTEST 2
#define BLUETEST 3
#define DOPPLEREFFECTVERTICAL 4
#define DOPPLEREFFECTHORIZONTAL 5
#define BLACKTEST 6
#define EXIT 7

#include <QWidget>
#include <QApplication>
#include <QPainter>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QIcon>



#include <QDebug>

class ScreenTestWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ScreenTestWindow(QWidget *parent = 0);
    ~ScreenTestWindow();

    void nextScreen(bool revers=false);

private:
    int screenType;

protected:
    void keyPressEvent(QKeyEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void paintEvent (QPaintEvent *);

};

#endif // SCREENTESTWINDOW_H
